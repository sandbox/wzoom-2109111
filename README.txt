Please set the Google Maps API key first:
/admin/config/user-interface/city-autocomplete


== Usage: ==
  $form['city'] = array(
    '#type' => 'city_autocomplete',
    '#title' => t('City'),
  );


== Submitted values: ==
  array(
    'city' => 'Prague',
    'country' => 'Czech Republic',
    'country_code' => 'CZ',
    'lat' => 50.0755381,
    'lon' => 14.4378005,
    'value' => 'Prague, Czech Republic',
    'formatted' => 'Prague, Czech Republic',
  );


The City Autocomplete DOES respect user's language settings.
Other values can be added from the Places API upon request.